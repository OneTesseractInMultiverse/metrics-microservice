from influxdb import InfluxDBClient
from influxdb.resultset import ResultSet

json_body = [
    {
        "measurement": "test_cpu_load_short",
        "tags": {
            "host": "server01",
            "region": "us-west"
        },
        "time": "2009-11-10T23:00:00Z",
        "fields": {
            "value": 0.64
        }
    }
]

client = InfluxDBClient(
    'influx.thehackingdojo.com',
    8086,
    'admin',
    '6a3eb89dfd62113efa4ebc9ee7345a43e8c56363348f5898',
    'example'
)

client.create_database('example')
# client.write_points(json_body)
result: ResultSet = client.query('select value from cpu;')
cpu_points = list(result.get_points(measurement='cpu'))
print("Result: {0}".format(cpu_points))