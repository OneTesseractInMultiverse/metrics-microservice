from fastapi import APIRouter
from typing import Dict, List
from app.schemas import CreateDiskMetric, DiskMetric
import app.influx_connection as db
import app.crud.disk_data_operations as crud

router = APIRouter()


@router.post('/disk_metric', tags=['DISK'], response_model=Dict)
async def submit_disk_metric(metrics: List[CreateDiskMetric]):
    for metric in metrics:
        crud.save_disk_metric(metric, db.get_connection())
    return {
        "status": "saved"
    }


@router.get('/host/{host_name}/disk_metric/{miner_id}', tags=['DISK'], response_model=List[Dict])
async def get_disk_metrics(host_name: str, miner_id: str):
    return crud.get_disk_metrics(
        db.get_connection(),
        miner_id=miner_id,
        host_name=host_name
    )
