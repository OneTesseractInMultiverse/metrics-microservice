from fastapi import APIRouter
from typing import Dict, List
from app.schemas import CreateNetMetric, NetMetric
import app.influx_connection as db
import app.crud.network_data_operations as crud

router = APIRouter()


@router.post('/network_metric', tags=['NETWORK'], response_model=Dict)
async def submit_network_metric(metrics: List[CreateNetMetric]):
    for metric in metrics:
        crud.save_network_metric(metric, db.get_connection())
    return {
        "status": "saved"
    }


@router.get('/host/{host_name}/network_metric/{miner_id}', tags=['NETWORK'], response_model=List[Dict])
async def get_network_metrics(host_name: str, miner_id: str):
    return crud.get_network_metrics(
        db.get_connection(),
        miner_id=miner_id,
        host_name=host_name
    )
