import app.schemas as schemas
from influxdb import InfluxDBClient
from influxdb.resultset import ResultSet
import pprint


def get_disk_metrics(connection: InfluxDBClient, miner_id: str, host_name: str):
    # TODO This code is vulnerable to SQL Injection
    sql = """SELECT value FROM disk WHERE miner_id='{0}' and host_name='{1}';""".format(miner_id, host_name)
    result: ResultSet = connection.query(sql)
    pprint.pprint(result)
    disk_points = list(result.get_points(measurement='disk'))
    return disk_points


def save_disk_metric(metric: schemas.CreateDiskMetric, connection: InfluxDBClient):
    data = metric.dict()
    data['fields'] = data['captures']
    del data['captures']
    points: list = [data]
    return connection.write_points(points)

