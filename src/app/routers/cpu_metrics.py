from fastapi import APIRouter
from typing import Dict, List
from app.schemas import CreateCpuMetric, CpuMetric
import app.influx_connection as db
import app.crud.cpu_data_operations as crud

router = APIRouter()


@router.post('/cpu_metric', tags=['CPU'], response_model=Dict)
async def submit_cpu_metric(metrics: List[CreateCpuMetric]):
    for metric in metrics:
        crud.save_cpu_metric(metric, db.get_connection())
    return {
        "status": "saved"
    }


@router.get('/host/{host_name}/cpu_metric/{miner_id}', tags=['CPU'], response_model=List[Dict])
async def get_cpu_metrics(host_name: str, miner_id: str):
    return crud.get_cpu_metrics(
        db.get_connection(),
        miner_id=miner_id,
        host_name=host_name
    )
