from fastapi import FastAPI
from .routers import cpu_metrics, memory_metrics, network_metrics, disk_metrics

app = FastAPI(
    title='Metrics Microservice',
    description='A Python-based microservice that expose an web interface to capture system performance metrics', 
    version='1.0.0', 
    openapi_url='/api/openapi.json',
    docs_url='/api/docs',
    redoc_url=None
)

app.include_router(cpu_metrics.router, prefix='/api/v1')
app.include_router(memory_metrics.router, prefix='/api/v1')
app.include_router(disk_metrics.router, prefix='/api/v1')
app.include_router(network_metrics.router, prefix='/api/v1')


