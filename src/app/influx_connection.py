import app.config as config
from influxdb import InfluxDBClient


def get_connection() -> InfluxDBClient:
    client = InfluxDBClient(
        config.INFLUX_HOST,
        8086,
        config.INFLUX_USER,
        config.INFLUX_PASSWORD,
        config.INFLUX_DB
    )
    client.create_database(config.INFLUX_DB)
    return client
