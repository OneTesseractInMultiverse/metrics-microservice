from fastapi import APIRouter
from typing import Dict, List
from app.schemas import CreateMemMetric, MemMetric
import app.influx_connection as db
import app.crud.memory_data_operations as crud

router = APIRouter()


@router.post('/memory_metric', tags=['MEMORY'], response_model=Dict)
async def submit_memory_metric(metrics: List[CreateMemMetric]):
    for metric in metrics:
        crud.save_memory_metric(metric, db.get_connection())
    return {
        "status": "saved"
    }


@router.get('/host/{host_name}/memory_metric/{miner_id}', tags=['MEMORY'], response_model=List[Dict])
async def get_memory_metrics(host_name: str, miner_id: str):
    return crud.get_memory_metrics(
        db.get_connection(),
        miner_id=miner_id,
        host_name=host_name
    )
