#!/usr/bin/env bash
docker ps -a -f status=exited
docker system prune -f
docker rmi metrics-server
docker build -t metrics-server .
docker run --detach --name metrics-server \
    -p 80:80 \
    -e "VIRTUAL_HOST=metrics.appsecurity.info" \
    -e "LETSENCRYPT_HOST=metrics.appsecurity.info" \
    -e "DATABASE_URL={}" \
    metrics-server
