from typing import List, Dict
from pydantic import Field, BaseModel
import uuid


# -----------------------------------------------------------------------------
# METRIC FIELD
# -----------------------------------------------------------------------------
class MetricField(BaseModel):
    value: float = Field(None, title='The actual value of the captured metric')


# -----------------------------------------------------------------------------
# STORED METRIC
# -----------------------------------------------------------------------------
class MetricTags(BaseModel):
    miner_id: str = Field(None, title='The unique id of each miner implementation. It is one per team',)
    host_name: str = Field(None, title='name of the computer where the miner is running')
    instance_id: str = Field(None, title='Unique identifier for the running instance')


# -----------------------------------------------------------------------------
# STORED METRIC
# -----------------------------------------------------------------------------
class BaseStoredMetric(BaseModel):
    measurement: str = Field(None, title='Type of measurement being represented')
    tags: MetricTags = Field(None, title="Tags for this metric")
    time: str = Field(None, title="The timestamp when the metric was captured")
    captures: MetricField = Field(None, title='Contains the actual value of the metric')


# -----------------------------------------------------------------------------
# BASE CREATE METRIC
# -----------------------------------------------------------------------------
class BaseCreateMetricSchema(BaseModel):
    measurement: str = Field(None, title='Type of measurement being represented')
    tags: MetricTags = Field(None, title="Tags for this metric")
    time: str = Field(None, title="The timestamp when the metric was captured")
    captures: MetricField = Field(None, title='Contains the actual value of the metric')


# -----------------------------------------------------------------------------
# CPU METRIC
# -----------------------------------------------------------------------------
class CreateCpuMetric(BaseCreateMetricSchema):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.measurement = 'cpu'


# -----------------------------------------------------------------------------
# CPU METRIC
# -----------------------------------------------------------------------------
class CpuMetric(BaseStoredMetric):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


# -----------------------------------------------------------------------------
# MEM METRIC
# -----------------------------------------------------------------------------
class CreateMemMetric(BaseCreateMetricSchema):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.measurement = 'memory'


# -----------------------------------------------------------------------------
# MEM METRIC
# -----------------------------------------------------------------------------
class MemMetric(BaseStoredMetric):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


# -----------------------------------------------------------------------------
# NET METRIC
# -----------------------------------------------------------------------------
class CreateNetMetric(BaseCreateMetricSchema):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.measurement = 'network'


# -----------------------------------------------------------------------------
# NET METRIC
# -----------------------------------------------------------------------------
class NetMetric(BaseStoredMetric):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


# -----------------------------------------------------------------------------
# DISK METRIC
# -----------------------------------------------------------------------------
class CreateDiskMetric(BaseCreateMetricSchema):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.measurement = 'disk'


# -----------------------------------------------------------------------------
# DISK METRIC
# -----------------------------------------------------------------------------
class DiskMetric(BaseStoredMetric):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
