import os

INFLUX_USER: str = os.environ.get('INFLUX_USER')
INFLUX_PASSWORD: str = os.environ.get('INFLUX_PASSWORD')
INFLUX_DB: str = os.environ.get('INFLUX_DB')
INFLUX_HOST: str = os.environ.get('INFLUX_HOST')