import app.schemas as schemas
from influxdb import InfluxDBClient
from influxdb.resultset import ResultSet
import pprint


def get_cpu_metrics(connection: InfluxDBClient, miner_id: str, host_name: str):
    # TODO This code is vulnerable to SQL Injection
    sql = """SELECT value FROM cpu WHERE miner_id='{0}' and host_name='{1}';""".format(miner_id, host_name)
    result: ResultSet = connection.query(sql)
    cpu_points = list(result.get_points(measurement='cpu'))
    return cpu_points


def save_cpu_metric(metric: schemas.CreateCpuMetric, connection: InfluxDBClient):
    data = metric.dict()
    data['fields'] = data['captures']
    del data['captures']
    points: list = [data]
    return connection.write_points(points)

