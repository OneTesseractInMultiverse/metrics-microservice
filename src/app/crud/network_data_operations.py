import app.schemas as schemas
from influxdb import InfluxDBClient
from influxdb.resultset import ResultSet
import pprint


def get_network_metrics(connection: InfluxDBClient, miner_id: str, host_name: str):
    # TODO This code is vulnerable to SQL Injection
    sql = """SELECT value FROM network WHERE miner_id='{0}' and host_name='{1}';""".format(miner_id, host_name)
    result: ResultSet = connection.query(sql)
    pprint.pprint(result)
    network_points = list(result.get_points(measurement='network'))
    return network_points


def save_network_metric(metric: schemas.CreateNetMetric, connection: InfluxDBClient):
    data = metric.dict()
    data['fields'] = data['captures']
    del data['captures']
    points: list = [data]
    return connection.write_points(points)

